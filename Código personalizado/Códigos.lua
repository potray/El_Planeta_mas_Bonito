--Códigos de páginas

--Favores
pag_favor1 = 7 	
pag_favor2 = 36	
pag_favor3 = 52
pag_favor4 = 64
pag_favor5 = 72
pag_favor6 = 80
pag_favor7 = 84
pag_burbuja1 = 37	
pag_burbuja2 = 73

--Objetos
pag_tijeras = 2  	--Tijeras y biberón
pag_rosa = 11 		--Rosa
pag_erizo = 25 		--Erizo
pag_maquina = 31 	--Maquina de agrandar cosas
pag_guitarra = 35 	--Guitarra
pag_cola = 49		--Cola blanca
pag_celo = 71		--Celo

--Preguntas
pag_pregunta1 = 22	--Pregunta 1
pag_pregunta2 = 33	--Pregunta 2
pag_pregunta3 = 42	--Pregunta 3
pag_pregunta4 = 45	--Pregunta 4
pag_pregunta5 = 50 	--Pregunta 5
pag_pregunta6 = 55	--Pregunta 6
pag_pregunta7 = 57	--Pregunta 7
pag_pregunta8 = 78	--Pregunta 8
